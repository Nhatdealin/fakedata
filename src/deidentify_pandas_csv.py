from faker import Faker
import pandas as pd
import os

faker = Faker()
list_id = []
def change_data(row):
    row['subscriber_last_name'] = faker.last_name()
    row['subscriber_first_name'] = faker.first_name()
    row['subscriber_middle_name_or_initial'] = faker.last_name()
    row['subscriber_name_suffix'] = faker.suffix()
    rand_number = faker.random_number(10, True)
    while rand_number in list_id : 
        rand_number = faker.random_number(10, True)
    row['subscriber_primary_identifier'] = rand_number
    list_id.append(rand_number)
    row['subscriber_address_line'] = faker.address()
    row['subscriber_address_line.1'] = faker.secondary_address()
    row['subscriber_state_code'] = faker.state_abbr(include_territories=True)
    row['subscriber_postal_zone_or_zip_code'] = faker.postalcode_plus4()
    row['subscriber_birth_date'] = faker.date_of_birth(tzinfo=None, minimum_age=41).strftime("%Y%m%d")
    row['subscriber_gender_code'] = faker.random_element(elements = ('M', 'L', 'OTHER'))
    return row
def seed_data_csv(file_csv_name,root_output):
    number = 0
    result_file = "{}/change_{}_{}".format(root_output, str(number), os.path.basename(file_csv_name)) 
    while(os.path.exists(result_file)):
        number += 1
        result_file = "{}/change_{}_{}".format(root_output, str(number), os.path.basename(file_csv_name))
    df = pd.read_csv(file_csv_name).head(10)
    list_key = list(map(lambda x: "subscriber_address_line" if x == 'subscriber_address_line.1' else x, df.columns.to_list()))  
    print(list_key)
    df_de = df.apply(change_data, axis = 1)
    df_de.columns = list_key
    df_de.to_csv(result_file,index=False)
    list_id = []
if __name__ == '__main__' :
    seed_data_csv('../input/2010BA_DEIDENT_INTHB0837I_286230_10160249.csv','../output')
    # seed_data_csv('../input/2010BA_DEIDENT_INTPSV837P_286193_10140229.csv','../output')