from faker import Faker
import csv
import os

faker = Faker()

def mappingdict(list_keys,list_values):
    dict_item = dict()
    is_first_address = True
    for i in range(0,len(list_keys)) :
        
        if list_keys[i] == 'subscriber_address_line' : 
            if is_first_address == True :
                dict_item['subscriber_address_line_1'] = list_values[i]
                print(i)
                is_first_address = False                
            else : 
                dict_item['subscriber_address_line_2'] = list_values[i]
                print(i)
        else: 
            dict_item[list_keys[i]] = list_values[i]
    return dict_item

def seed_data_csv(file_csv_name,root_output):
    with open(file_csv_name, mode='r') as csv_file:
        number = 0
        result_file = "{}/change_{}_{}".format(root_output, str(number), os.path.basename(file_csv_name)) 
        while(os.path.exists(result_file)):
            number += 1
            result_file = "{}/change_{}_{}".format(root_output, str(number), os.path.basename(file_csv_name))
        with open(result_file,'w') as f:
            reader = csv.reader(csv_file)
            writer = csv.writer(f)
            is_first = True
            list_id = []
            for row in reader:
                if is_first == True:
                    writer.writerow(row)
                    list_key = row
                    is_first = False
                else:
                    dict_item = mappingdict(list_key,row)
                    dict_item['subscriber_last_name'] = faker.last_name()
                    dict_item['subscriber_first_name'] = faker.first_name()
                    dict_item['subscriber_middle_name_or_initial'] = faker.last_name()
                    dict_item['subscriber_name_suffix'] = faker.suffix()
                    rand_number = faker.random_number(10, True)
                    while rand_number in list_id : 
                        rand_number = faker.random_number(10, True)
                    dict_item['subscriber_primary_identifier'] = rand_number
                    list_id.append(rand_number)
                    dict_item['subscriber_address_line_1'] = faker.address()
                    dict_item['subscriber_address_line_2'] = faker.secondary_address()
                    dict_item['subscriber_state_code'] = faker.state_abbr(include_territories=True)
                    dict_item['subscriber_postal_zone_or_zip_code'] = faker.postalcode_plus4()
                    dict_item['subscriber_birth_date'] = faker.date_of_birth(tzinfo=None, minimum_age=41).strftime("%Y%m%d")
                    dict_item['subscriber_gender_code'] = faker.random_element(elements = ('M', 'L', 'OTHER'))
                    writer.writerow(dict_item.values())
        f.close()
    csv_file.close()

if __name__ == '__main__' :
    seed_data_csv('../input/2010BA_DEIDENT_INTHB0837I_286230_10160249.csv','../output')
    seed_data_csv('../input/2010BA_DEIDENT_INTPSV837P_286193_10140229.csv','../output')