from faker import Faker
import csv
import os

faker = Faker()

def seed_data_txt(file_txt_name,root_output):
    with open(file_txt_name, mode='r') as txt_file:
        number = 0
        result_file = "{}/change_{}_{}".format(root_output, str(number), os.path.basename(file_txt_name))
        while(os.path.exists(result_file)):
            number += 1
            result_file = "{}/change_{}_{}".format(root_output, str(number), os.path.basename(file_txt_name))
        with open(result_file,'w') as f:
            reader = txt_file.readlines()
            for row in reader:
                list_str = row.split('*')
                list_str[3] = faker.first_name()
                list_str[4] = faker.last_name()
                list_str[9] = str(faker.random_number(11, True)) + '~'
                s = '*'
                f.write(s.join(list_str) + '\n')
        f.close()
    txt_file.close()
if __name__ == '__main__' :
    seed_data_txt('./input/subscriber_name.txt','./output')